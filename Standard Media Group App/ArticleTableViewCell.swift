//
//  ArticleTableViewCell.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 11/03/2021.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    @IBOutlet weak var footer: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
