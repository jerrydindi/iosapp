//
//  UIView+DMAdditions.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 10/01/2021.
//

import Foundation
import UIKit

extension UIView {

  func usesAutolayout(_ usesAutolayout: Bool) {
    translatesAutoresizingMaskIntoConstraints = !usesAutolayout
  }
  
}
