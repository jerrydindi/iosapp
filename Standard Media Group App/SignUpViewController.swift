//
//  SignUpViewController.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 07/01/2021.
//

import UIKit

class SignUpViewController: UIViewController {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPwd: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtTerms: UILabel!
    @IBOutlet weak var lblSignin: UILabel!
    @IBOutlet weak var btnSignup: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var count = 1
    
    var iconClick = true
    
    var imageViewEyePwd = UIImageView()
    
    @IBAction func btnSignup(_ sender: Any) {
        /* let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "sbLogin")
         vc.modalPresentationStyle = .fullScreen
         self.present(vc, animated: true) */
        let email = txtEmail.text!
        let phone = txtPhone.text!
        let pwd = txtPwd.text!
        let name = txtName.text!
        
        if (email == "" || pwd == "" || name == "")
        {
            showAlert(title: "Sign up", message: "Fill in all details")
        }
        else if (phone.count < 10)
        {
            showAlert(title: "Sign up", message: "Invalid phone number")
        }
        else
        {
            // make call to API
           /* showSpinner()
            let url = appDelegate.URL_BASE + appDelegate.URL_REGISTER
            let params = "email=" + email + "&phone=" + phone + "&password=" + pwd + "&name=" + name.replacingOccurrences(of: " ", with: "%20")
            
            appDelegate.doPost(url: url, module: appDelegate.URL_REGISTER, paramz: params) */
            
            self.showAlert(title: "Sign up", message: "Successfully Registered")
            
            //  dismiss(animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        var imageViewName = UIImageView()
        var name = UIImage(systemName: "person")
        imageViewName.tintColor = UIColor.lightGray
        imageViewName.image = name
        
        txtName.leftView = imageViewName
        txtName.leftViewMode = UITextField.ViewMode.always
        txtName.leftViewMode = .always
        
        var imageView = UIImageView()
        var image = UIImage(systemName: "envelope")
        imageView.tintColor = UIColor.lightGray
        imageView.image = image
        txtEmail.leftView = imageView
        txtEmail.leftViewMode = UITextField.ViewMode.always
        txtEmail.leftViewMode = .always
        
        var phone = UIImage(systemName: "phone")
        var imageViewPhone = UIImageView()
        imageViewPhone.tintColor = UIColor.lightGray
        imageViewPhone.image = phone
        
        txtPhone.leftView = imageViewPhone
        txtPhone.leftViewMode = UITextField.ViewMode.always
        txtPhone.leftViewMode = .always
        
        var eye = UIImage(systemName: "eye")
        imageViewEyePwd.tintColor = UIColor.darkGray
        imageViewEyePwd.image = eye
        
        txtPwd.rightView = imageViewEyePwd
        txtPwd.rightViewMode = UITextField.ViewMode.always
        txtPwd.rightViewMode = .always
        
        var imageViewLock = UIImageView()
        var lock = UIImage(systemName: "lock")
        imageViewLock.tintColor = UIColor.lightGray
        imageViewLock.image = lock
        
        txtPwd.leftView = imageViewLock
        txtPwd.leftViewMode = UITextField.ViewMode.always
        txtPwd.leftViewMode = .always
        
        var imageViewLockConf = UIImageView()
        imageViewLockConf.tintColor = UIColor.lightGray
        imageViewLockConf.image = lock
        
        let tapGestureRecognizer_pwd = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        
        imageViewEyePwd.isUserInteractionEnabled = true
        imageViewEyePwd.addGestureRecognizer(tapGestureRecognizer_pwd)
        
        // Listen to Hide/Show keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // Notification from App Delegate
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived(_:)), name: NSNotification.Name(rawValue: appDelegate.URL_REGISTER), object: nil)
        
        let tap_signin = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.tapFunction))
        
        lblSignin.isUserInteractionEnabled = true
        lblSignin.addGestureRecognizer(tap_signin)
        
        let tap_terms = UITapGestureRecognizer(target: self, action: #selector(SigninViewController.tapFunction))
        
        txtTerms.isUserInteractionEnabled = true
        txtTerms.addGestureRecognizer(tap_terms)
        
        // Hide keyboard on screen tap
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        
        btnSignup.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .bold)
        btnSignup.backgroundColor = UIColor(red: 209/255, green: 50/255, blue: 41/255, alpha: 1) // red
        btnSignup.layer.cornerRadius = appDelegate.BtnCornnerRadius
        btnSignup.layer.borderWidth = appDelegate.BorderWidth
        btnSignup.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1).cgColor
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        switch(tappedImage)
        {
        case imageViewEyePwd:
            setPasswordView()
            break
        default:
            print("default")
            break
        }
    }
    
    func setPasswordView()
    {
        if(iconClick == true) {
            txtPwd.isSecureTextEntry = false
        } else {
            txtPwd.isSecureTextEntry = true
        }
        
        iconClick = !iconClick
    }
    
    // Called when keyboard shows
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            /* if self.view.frame.origin.y == 0 {
             self.view.frame.origin.y -= keyboardSize.height
             } */
            // view.frame.origin.y -= keyboardSize.height
            count += 1
            if (count<=2)
            {
                view.frame.origin.y -= keyboardSize.height/2
            }
            
            //  bottomConstraint = self.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -keyboardSize.height-20)
            //  btnSend.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -keyboardSize.height).isActive = true
            //   bottomConstraint.isActive = true
        }
    }
    
    // Called when keyboard is hidden
    @objc func keyboardWillHide(notification: NSNotification) {
        /* if self.view.frame.origin.y != 0 {
         self.view.frame.origin.y = 0
         } */
        //   btnSend.frame.origin.y = 0
        // bottomConstraint.isActive = false
        count = 1
        view.frame.origin.y = 0
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        
        let tappedLabel = sender.view as! UILabel
        
        switch(tappedLabel)
        {
        case lblSignin:
            dismiss(animated: true, completion: nil)
            break
        case txtTerms:
            if let url = URL(string: "https://www.standardmedia.co.ke/terms-and-conditions") {
                UIApplication.shared.open(url)
            }
            break
        default:
            print("tap working")
            break
        }
    }
    
    // Receive POST response
    @objc func notificationReceived(_ notification: NSNotification) {
        if let topic_ = notification.userInfo?["Topic"] as? String {
            print(topic_)
        }
        if let message = notification.userInfo?["message"] as? [String: Any] {
            print(message)
            decodeJSON(json: message)
            hideSpinner()
        }
    }
    
    func decodeJSON (json : [String: Any])
    {
        do {
            
            if let error = json["error"] as? Bool {
                
                if (error == false)
                {
                    print (json)
                    
                   /* if let user = json["user"] as? [String: Any]
                    {
                        let name = user["name"]!
                        let email = user["email"]!
                        let phone = user["phone"]!
                        let id = user["id"]!
                        
                        UserDefaults.standard.set(name, forKey: "name")
                        UserDefaults.standard.set(email, forKey: "email")
                        UserDefaults.standard.set(phone, forKey: "phone")
                        UserDefaults.standard.set(id, forKey: "id")
                        
                        // launchMain()
                    }
                    else
                    {
                        showAlert(title: "Sign up", message: "Invalid details")
                    } */
                }
                else{
                    // message
                    if let msg = json["message"] as? String {
                        
                        showAlert(title: "Sign up", message: msg)
                    }
                    
                }
                //   print(error)
            }
            else{
                showAlert(title: "Sign up", message: "Invalid details")
            }
            
        } catch {
            print(error)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
