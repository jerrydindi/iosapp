//
//  ContentVC.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 11/03/2021.
//

import UIKit
import KFSwiftImageLoader

class ContentVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var nav = UserDefaults.standard.string(forKey: "Navigation")
    
   // let data = ["One","Two","Three","Four","Five",]
    
    var data = [[String:AnyObject]].init()
    
    var pageIndex: Int = 0
    var strTitle: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = strTitle
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        if (strTitle == appDelegate.TITLE_MAIN_FAVOURITES)
        {
            
        }
        else if (strTitle == appDelegate.TITLE_MAIN_TOP_STORIES)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        else if (strTitle == appDelegate.TITLE_MAIN_POLITICS)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_MAIN_POLITICS_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_MAIN_LOCAL_NEWS)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_MAIN_LOCAL_NEWS_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_MAIN_ENTERTAINMENT)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_MAIN_ENTERTAINMENT_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        /* else if (strTitle == appDelegate.TITLE_MAIN_SLIDE_SHOW)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_MAIN_ENTERTAINMENT_SLIDESHOW_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        } */
        
        else if (strTitle == appDelegate.TITLE_BUSINESS_FINANCIAL_STANDARD)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_STANDARD_BUSINESS_FINANCIAL_STANDARD_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_BUSINESS_HUSTLE)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_STANDARD_BUSINESS_HUSTLE_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        else if (strTitle == appDelegate.TITLE_EVE_WOMAN_LIVING)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_EVE_WOMAN_LIVING_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_EVE_WOMAN_FASHION_BEAUTY)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_EVE_WOMAN_FASHION_BEAUTY_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_EVE_WOMAN_RELATIONSHIPS)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_EVE_WOMAN_RELATIONSHIPS_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_EVE_WOMAN_PARENTING)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_EVE_WOMAN_PARENTING_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_EVE_WOMAN_READERS_LOUNGE)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_EVE_WOMAN_READERS_LOUNGE_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_ENTERTAINMENT_NEWS && nav == "ENTERTAINMNET")
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_ENTERTAINMENT_NEWS_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_ENTERTAINMENT_NAIROBIAN)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_ENTERTAINMENT_NAIROBIAN_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_ENTERTAINMENT_PULSE)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_ENTERTAINMENT_PULSE_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_ENTERTAINMENT_CRAZY_MONDAY)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_ENTERTAINMENT_CRAZY_MONDAY_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_SPORTS)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_SPORTS_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_TRAVELOG)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_TRAVELOG_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_PULSER)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_PULSER_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_FARMERS_SMART_HARVEST)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_FARMERS_SMART_HARVEST_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_FARMERS_CROP)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_FARMERS_CROP_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_FARMERS_LIVESTOCK)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_FARMERS_LIVESTOCK_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_FARMERS_MARKET)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_FARMERS_MARKET_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_FARMERS_TECHNOLOGY)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_FARMERS_TECHNOLOGY_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_FARMER_RESEARCH)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_FARMERS_RESEARCH_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_FARMERS_OPEDS)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_FARMERS_OPEDS_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_FARMERS_ABOUT_US)
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_FARMERS_ABOUT_US_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_FARMERS_NEWS && nav == "FARMERS")
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_FARMERS_NEWS_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
        
        else if (strTitle == appDelegate.TITLE_FARMERS_EVENTS && nav == "FARMERS")
        {
           // data = UserDefaults.standard.object(forKey: appDelegate.URL_ARTICLES_MAIN_TOP_STORIES_ID) as! [[Any]]
            
            let decoded  = UserDefaults.standard.data(forKey: appDelegate.URL_ARTICLES_FARMERS_EVENTS_ID)
            data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [[String:AnyObject]]
            
            self.registerTableViewCells()
        }
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleTableViewCell") as? ArticleTableViewCell {
            
          /*  cell.title.text = "Article" + String(indexPath.item)
            cell.footer.text = "Footer" + String(indexPath.item)
            let urlString = "https://cdn.standardmedia.co.ke/images/friday/haji_on_the_spot_ove5f11f25858d41.jpg"
            cell.thumbnail.loadImage(urlString: urlString) */
            
          //  print(data[indexPath.item].author)
            
            if let article = data[indexPath.item] as? [String: Any]
              {
                //  print(article["author"])
                cell.title.text = article["title"] as! String
                cell.footer.text = article["publishdate"] as! String
                let urlString = article["thumbURL"] as! String
                cell.thumbnail.loadImage(urlString: urlString)
              }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let article = data[indexPath.item] as? [String: Any]
          {
            //  print(article["author"])
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: article)
            UserDefaults.standard.set(encodedData, forKey: "fullArticle")
            UserDefaults.standard.set(article["share"], forKey: "shareArticle")
            UserDefaults.standard.synchronize()
            
            performSegue(withIdentifier: "segueReadArticle", sender: nil)
          }
        
    }
    
    private func registerTableViewCells() {
        let textFieldCell = UINib(nibName: "ArticleTableViewCell",
                                  bundle: nil)
        self.tableView.register(textFieldCell,
                                forCellReuseIdentifier: "ArticleTableViewCell")
    }

}
