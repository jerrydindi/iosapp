//
//  CardView.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 11/03/2021.
//

import UIKit

@IBDesignable class CardView: UIView {
        var cornnerRadius : CGFloat = 2
        var shadowOfSetWidth : CGFloat = 0
        var shadowOfSetHeight : CGFloat = 5
        
    var shadowColour : UIColor = UIColor.darkGray
        var shadowOpacity : CGFloat = 0.5
        
        override func layoutSubviews() {
            layer.cornerRadius = cornnerRadius
            layer.shadowColor = shadowColour.cgColor
            layer.shadowOffset = CGSize(width: shadowOfSetWidth, height: shadowOfSetHeight)
            let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornnerRadius)
            layer.shadowPath = shadowPath.cgPath
            layer.shadowOpacity = Float(shadowOpacity)
        }
}
