//
//  SigninViewController.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 20/12/2020.
//

import UIKit
import AuthenticationServices

class SigninViewController: UIViewController {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPwd: UITextField!
    @IBOutlet weak var btnSignin: UIButton!
    @IBOutlet weak var txtTerms: UILabel!
    @IBOutlet weak var signInButtonStack: UIStackView!
    
    var bottomConstraint:NSLayoutConstraint!
    
    var email = ""
    var pwd = ""
    var count = 1
    
    var imageViewEye = UIImageView()
    
    var iconClick = true
    
  //  var userID = UserDefaults.standard.string(forKey: "id")
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBAction func btnSignin(_ sender: Any) {
        /* let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "sbMainDash")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true) */
        
        email = txtEmail.text!
        pwd = txtPwd.text!
        
        if (email == "" || pwd == "")
        {
            showAlert(title: "Sign in", message: "Fill in all details")
        }
        else
        {
            // make call to API
            //doPost(url: URL_BASE + URL_LOGIN,module: "login", paramz: "email=jerrydindi@gmail.com&password=123456789")
            showSpinner()
            let url = appDelegate.URL_BASE + appDelegate.URL_LOGIN
            let params = "email=" + email + "&password=" + pwd
            
            appDelegate.doPost(url: url, module: appDelegate.URL_LOGIN, paramz: params)
            
        }
    }
    
    @IBOutlet weak var btnSignup: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var imageView = UIImageView()
        var image = UIImage(systemName: "envelope")
        imageView.tintColor = UIColor.lightGray
        imageView.image = image
        txtEmail.leftView = imageView
        txtEmail.leftViewMode = UITextField.ViewMode.always
        txtEmail.leftViewMode = .always
        
        var eye = UIImage(systemName: "eye")
        imageViewEye.tintColor = UIColor.darkGray
        imageViewEye.image = eye
        
        txtPwd.rightView = imageViewEye
        txtPwd.rightViewMode = UITextField.ViewMode.always
        txtPwd.rightViewMode = .always
        
        var imageViewLock = UIImageView()
        var lock = UIImage(systemName: "lock")
        imageViewLock.tintColor = UIColor.lightGray
        imageViewLock.image = lock
        
        txtPwd.leftView = imageViewLock
        txtPwd.leftViewMode = UITextField.ViewMode.always
        txtPwd.leftViewMode = .always
        
        let tapGestureRecognizer_pwd = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        
        imageViewEye.isUserInteractionEnabled = true
        imageViewEye.addGestureRecognizer(tapGestureRecognizer_pwd)
        
        // Listen to Hide/Show keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // Notification from App Delegate
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived(_:)), name: NSNotification.Name(rawValue: appDelegate.URL_LOGIN), object: nil)
        
        UserDefaults.standard.set("Home", forKey: "Navigation")
        
        let tap_signup = UITapGestureRecognizer(target: self, action: #selector(SigninViewController.tapFunction))
        
        btnSignup.isUserInteractionEnabled = true
        btnSignup.addGestureRecognizer(tap_signup)
        
        let tap_terms = UITapGestureRecognizer(target: self, action: #selector(SigninViewController.tapFunction))
        
        txtTerms.isUserInteractionEnabled = true
        txtTerms.addGestureRecognizer(tap_terms)
        
        // Hide keyboard on screen tap
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        
        btnSignin.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .bold)
        btnSignin.backgroundColor = UIColor(red: 209/255, green: 50/255, blue: 41/255, alpha: 1) // red
        btnSignin.layer.cornerRadius = appDelegate.BtnCornnerRadius
        btnSignin.layer.borderWidth = appDelegate.BorderWidth
        btnSignin.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1).cgColor
        
        setUpSignInAppleButton()
        
       /* print("userID: " + userID!)
        
        if (userID == "0")
        {
            launchMain()
        } */
        
    }
    
    func setUpSignInAppleButton() {
      let authorizationButton = ASAuthorizationAppleIDButton()
     // authorizationButton.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
        let tap_auth = UITapGestureRecognizer(target: self, action: #selector(SigninViewController.handleAppleIdRequest))
        
        authorizationButton.isUserInteractionEnabled = true
        authorizationButton.addGestureRecognizer(tap_auth)
        
      authorizationButton.cornerRadius = 10
      //Add button on some view or stack
      self.signInButtonStack.addArrangedSubview(authorizationButton)
    }
    
    @objc func handleAppleIdRequest() {
    let appleIDProvider = ASAuthorizationAppleIDProvider()
    let request = appleIDProvider.createRequest()
    request.requestedScopes = [.fullName, .email]
    let authorizationController = ASAuthorizationController(authorizationRequests: [request])
    authorizationController.delegate = self
    authorizationController.performRequests()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        switch(tappedImage)
        {
        case imageViewEye:
            setPasswordView()
            break
        default:
            print("default")
            break
        }
    }
    
    func setPasswordView()
    {
        if(iconClick == true) {
            txtPwd.isSecureTextEntry = false
                } else {
                    txtPwd.isSecureTextEntry = true
                }

                iconClick = !iconClick
    }
    
    // Called when keyboard shows
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            /* if self.view.frame.origin.y == 0 {
             self.view.frame.origin.y -= keyboardSize.height
             } */
            // view.frame.origin.y -= keyboardSize.height
            count += 1
             if (count<=2)
             {
                view.frame.origin.y -= keyboardSize.height/2
             }
            
          //  bottomConstraint = self.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -keyboardSize.height-20)
            //  btnSend.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -keyboardSize.height).isActive = true
         //   bottomConstraint.isActive = true
        }
    }
    
    // Called when keyboard is hidden
    @objc func keyboardWillHide(notification: NSNotification) {
        /* if self.view.frame.origin.y != 0 {
         self.view.frame.origin.y = 0
         } */
        //   btnSend.frame.origin.y = 0
       // bottomConstraint.isActive = false
        count = 1
        view.frame.origin.y = 0
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {

        let tappedLabel = sender.view as! UILabel
        
        switch(tappedLabel)
        {
        case btnSignup:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "sbSignup")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true)
            break
        case txtTerms:
            if let url = URL(string: "https://www.standardmedia.co.ke/terms-and-conditions") {
                UIApplication.shared.open(url)
            }
            break
        default:
            print("tap working")
            break
        }
    }
    
    // Receive POST response
    @objc func notificationReceived(_ notification: NSNotification) {
        if let topic_ = notification.userInfo?["Topic"] as? String {
            print(topic_)
        }
        if let message = notification.userInfo?["message"] as? [String: Any] {
            print(message)
            decodeJSON(json: message)
            hideSpinner()
        }
    }
    
    func decodeJSON (json : [String: Any])
    {
        do {
            
            if let error = json["error"] as? Bool {
                  if (error == false)
                  {
                      if let user = json["user"] as? [String: Any]
                      {
                        let name = user["name"]!
                        let email = user["email"]!
                        let phone = user["phone"]!
                        let id = user["id"]!
                        
                        UserDefaults.standard.set(name, forKey: "name")
                        UserDefaults.standard.set(email, forKey: "email")
                        UserDefaults.standard.set(phone, forKey: "phone")
                        UserDefaults.standard.set(id, forKey: "id")
                        
                        launchMain()
                      }
                    else
                      {
                        showAlert(title: "Sign in", message: "Invalid details")
                      }
                  }
                  else{
                    showAlert(title: "Sign in", message: "Invalid details")
                  }
                          print(error)
                      }
            else{
                showAlert(title: "Sign in", message: "Invalid details")
            }
            
        } catch {
            print(error)
        }
    }
    
    func launchMain()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "sbMainDash")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SigninViewController: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
}

extension SigninViewController: ASAuthorizationControllerDelegate {
    // Handle authorization success
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Success
            if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            print("User id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))") }
            
            launchMain()
        }
    }

    // Handle authorization failure
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Authorization Failed: \(error)")
        showAlert(title: "Sign in", message: "Invalid details")
    }
}
