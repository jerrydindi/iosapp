//
//  LeftSideMenuViewController.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 10/01/2021.
//

import UIKit

class LeftSideMenuViewController: UIViewController {

    @IBOutlet weak var lblHome: UILabel!
    
    
    @IBOutlet weak var lblTheStandard: UILabel!
    @IBOutlet weak var lblStandardEpaper: UILabel!
    @IBOutlet weak var lblStandardBusiness: UILabel!
    @IBOutlet weak var lblSports: UILabel!
    
    @IBOutlet weak var lblEveWoman: UILabel!
    @IBOutlet weak var lblTravelog: UILabel!
    @IBOutlet weak var lblEntertainment: UILabel!
    @IBOutlet weak var lblPulser: UILabel!
    
    @IBOutlet weak var lblStandardVideos: UILabel!
    
    @IBOutlet weak var lblDiggerClassifieds: UILabel!
    @IBOutlet weak var lblFarmKenya: UILabel!
    @IBOutlet weak var lblVybezRadio: UILabel!
    @IBOutlet weak var lblSpiceFM: UILabel!
    @IBOutlet weak var lblRadioMaisha: UILabel!
    @IBOutlet weak var lblBTV: UILabel!
    @IBOutlet weak var lblKTNHOME: UILabel!
    @IBOutlet weak var lblKTNNEWS: UILabel!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // Receive GET response
    @objc func notificationReceived(_ notification: NSNotification) {
        if let topic_ = notification.userInfo?["Topic"] as? String {
            print(topic_)
        }
        if let message = notification.userInfo?["message"] as? [String: Any] {
            print(message)
            decodeJSON(json: message)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Notification from App Delegate
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived(_:)), name: NSNotification.Name(rawValue: "KTN_NEWS"), object: nil)
        
        let tap_vybz = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblVybezRadio.isUserInteractionEnabled = true
        lblVybezRadio.addGestureRecognizer(tap_vybz)
        
        let tap_ktnhome = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblKTNHOME.isUserInteractionEnabled = true
        lblKTNHOME.addGestureRecognizer(tap_ktnhome)
        
        let tap_btv = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblBTV.isUserInteractionEnabled = true
        lblBTV.addGestureRecognizer(tap_btv)
        
        let tap_spicefm = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblSpiceFM.isUserInteractionEnabled = true
        lblSpiceFM.addGestureRecognizer(tap_spicefm)
        
        let tap_ktnnewss = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblKTNNEWS.isUserInteractionEnabled = true
        lblKTNNEWS.addGestureRecognizer(tap_ktnnewss)
        
        let tap_radiomaisha = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblRadioMaisha.isUserInteractionEnabled = true
        lblRadioMaisha.addGestureRecognizer(tap_radiomaisha)
        
        let tap_thestandard = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblTheStandard.isUserInteractionEnabled = true
        lblTheStandard.addGestureRecognizer(tap_thestandard)
        
        let tap_thestandardepaper = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblStandardEpaper.isUserInteractionEnabled = true
        lblStandardEpaper.addGestureRecognizer(tap_thestandardepaper)
        
        let tap_farmkenya = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblFarmKenya.isUserInteractionEnabled = true
        lblFarmKenya.addGestureRecognizer(tap_farmkenya)
        
        let tap_diggerclassifieds = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblDiggerClassifieds.isUserInteractionEnabled = true
        lblDiggerClassifieds.addGestureRecognizer(tap_diggerclassifieds)
        
        let tap_Home = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblHome.isUserInteractionEnabled = true
        lblHome.addGestureRecognizer(tap_Home)
        
        let tap_The_Standard = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblTheStandard.isUserInteractionEnabled = true
        lblTheStandard.addGestureRecognizer(tap_The_Standard)
        
        let tap_Business = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblStandardBusiness.isUserInteractionEnabled = true
        lblStandardBusiness.addGestureRecognizer(tap_Business)
        
        let tap_Eve_Woman = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblEveWoman.isUserInteractionEnabled = true
        lblEveWoman.addGestureRecognizer(tap_Eve_Woman)
        
        let tap_Travelog = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblTravelog.isUserInteractionEnabled = true
        lblTravelog.addGestureRecognizer(tap_Travelog)
        
        let tap_Sports = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblSports.isUserInteractionEnabled = true
        lblSports.addGestureRecognizer(tap_Sports)
        
        let tap_Entertainment = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblEntertainment.isUserInteractionEnabled = true
        lblEntertainment.addGestureRecognizer(tap_Entertainment)
        
        let tap_Videos = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblStandardVideos.isUserInteractionEnabled = true
        lblStandardVideos.addGestureRecognizer(tap_Videos)
        
        let tap_Pulser = UITapGestureRecognizer(target: self, action: #selector(LeftSideMenuViewController.tapFunction))
        
        lblPulser.isUserInteractionEnabled = true
        lblPulser.addGestureRecognizer(tap_Pulser)
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {

        let tappedLabel = sender.view as! UILabel
        
        switch(tappedLabel)
        {
        case lblHome:
            loadMainDash(nav: lblHome.text!)
            break
        case lblTheStandard:
            loadMainDash(nav: lblTheStandard.text!)
            break
        case lblStandardBusiness:
            loadMainDash(nav: lblStandardBusiness.text!)
            break
        case lblEveWoman:
            loadMainDash(nav: lblEveWoman.text!)
            break
        case lblTravelog:
            loadMainDash(nav: lblTravelog.text!)
            break
        case lblSports:
            loadMainDash(nav: lblSports.text!)
            break
        case lblEntertainment:
            loadMainDash(nav: lblEntertainment.text!)
            break
        case lblStandardVideos:
            loadMainDash(nav: lblStandardVideos.text!)
            break
        case lblPulser:
            loadMainDash(nav: lblPulser.text!)
            break
        case lblVybezRadio:
            UserDefaults.standard.set("x7eob6j", forKey: "DMChannel")
            performSegue(withIdentifier: "segueDMVideo", sender: nil)
            break
        case lblKTNHOME:
            UserDefaults.standard.set("x7l3lxv", forKey: "DMChannel")
            performSegue(withIdentifier: "segueDMVideo", sender: nil)
            break
        case lblKTNNEWS:
           // showSpinner()
          //  appDelegate.doGET(url: appDelegate.URL_BASE + appDelegate.URL_KTN_NEWS, functn: "KTN_NEWS")
            let videoId = UserDefaults.standard.string(forKey: "KTN_NEWS")
            UserDefaults.standard.set(videoId, forKey: "YTChannel")
            performSegue(withIdentifier: "segueYT", sender: nil)
            break
        case lblBTV:
            UserDefaults.standard.set("x7h8c4c", forKey: "DMChannel")
            performSegue(withIdentifier: "segueDMVideo", sender: nil)
            break
        case lblRadioMaisha:
            // RadioMaisha
            UserDefaults.standard.set("http://radiomaisha-atunwadigital.streamguys1.com/radiomaisha", forKey: "RadioMaisha")
            //performSegue(withIdentifier: "segueRadioMaisha", sender: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "sbRadioMaisha")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true)
            break
        case lblSpiceFM:
            UserDefaults.standard.set("x7eo9oe", forKey: "DMChannel")
            performSegue(withIdentifier: "segueDMVideo", sender: nil)
            break
        case lblStandardEpaper:
            if let url = URL(string: "https://apps.apple.com/ke/app/the-standard-digital-e-paper/id1384640649") {
                UIApplication.shared.open(url)
            }
            break
        case lblFarmKenya:
           /* if let url = URL(string: "https://apps.apple.com/ke/app/the-standard-digital-e-paper/id1384640649") {
                UIApplication.shared.open(url)
            } */
            loadMainDash(nav: "FARMERS")
            break
        case lblDiggerClassifieds:
            if let url = URL(string: "https://digger.standardmedia.co.ke") {
                UIApplication.shared.open(url)
            }
            break
        default:
            print("tap working")
            break
        }
        
    }
    
    func decodeJSON (json : [String: Any])
    {
        do {
            
            if let videoId = json["videoId"] as? String {
                print(videoId)
               // hideSpinner()
                UserDefaults.standard.set(videoId, forKey: "YTChannel")
                performSegue(withIdentifier: "segueYT", sender: nil)
            }
          //  print(json)
        } catch {
            print(error)
        }
    }
    
    func loadMainDash(nav: String)
    {
        UserDefaults.standard.set(nav, forKey: "Navigation")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "sbMainDash")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
}
