//
//  Article.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 17/03/2021.
//

import Foundation

class Article: NSObject, NSCoding {
    var id: Int
    var title: String
    var author: String
    var categoryid: Int
    var inactive: String
    var story: String
    var source: String
    var thumbURL: String
    var publishdate: String
    var summary: String
    var share: String

    init(id: Int, title: String, author: String, categoryid: Int, inactive: String, story: String, source: String, thumbURL: String, publishdate: String, summary: String, share: String) {
        
        self.id = id
        self.title = title
        self.author = author
        self.categoryid = categoryid
        self.inactive = inactive
        self.story = story
        self.source = source
        self.thumbURL = thumbURL
        self.publishdate = publishdate
        self.summary = summary
        self.share = share

    }

    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeInteger(forKey: "id")
        let title = aDecoder.decodeObject(forKey: "title") as! String
        let author = aDecoder.decodeObject(forKey: "author") as! String
        let categoryid = aDecoder.decodeInteger(forKey: "categoryid")
        let inactive = aDecoder.decodeObject(forKey: "inactive") as! String
        let story = aDecoder.decodeObject(forKey: "story") as! String
        let source = aDecoder.decodeObject(forKey: "source") as! String
        let thumbURL = aDecoder.decodeObject(forKey: "thumbURL") as! String
        let publishdate = aDecoder.decodeObject(forKey: "publishdate") as! String
        let summary = aDecoder.decodeObject(forKey: "summary") as! String
        let share = aDecoder.decodeObject(forKey: "share") as! String
        self.init(id: id, title: title, author: author, categoryid: categoryid,inactive: inactive, story: story, source: source, thumbURL: thumbURL, publishdate: publishdate, summary: summary, share: share)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(author, forKey: "author")
        aCoder.encode(categoryid, forKey: "categoryid")
        aCoder.encode(inactive, forKey: "inactive")
        aCoder.encode(story, forKey: "story")
        aCoder.encode(source, forKey: "source")
        aCoder.encode(thumbURL, forKey: "thumbURL") // publishdate
        aCoder.encode(publishdate, forKey: "publishdate")
        aCoder.encode(summary, forKey: "summary")
        aCoder.encode(share, forKey: "share")
    }
}
