//
//  MainDashViewController.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 08/01/2021.
//

import UIKit

class MainDashViewController: UIViewController {

    @IBAction func menu(_ sender: Any) {
        performSegue(withIdentifier: "segueMenu", sender: nil)
    }
    
    @IBAction func logout(_ sender: Any) {
        // remove user details
        UserDefaults.standard.removeObject(forKey: "name")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "phone")
        UserDefaults.standard.removeObject(forKey: "id")
        
        // remove all keys
      /*  if let appDomain = Bundle.main.bundleIdentifier {
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
         } */
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "sbLogin")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    @IBOutlet weak var menuBarView: MenuTabsView!
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var navBarItem: UINavigationItem!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var currentIndex: Int = 0
    var tabs = ["TOP STORIES","FAVOURITES","LOCAL NEWS","POLITICS","ENTERTAINMENT","SLIDE SHOW"]
    var pageController: UIPageViewController!
    
    var nav = UserDefaults.standard.string(forKey: "Navigation")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (nav!.caseInsensitiveCompare("Home") == .orderedSame)
        {
            nav = "THE STANDARD"
        }
        else if (nav == "THE STANDARD BUSINESS")
        {
            tabs = [appDelegate.TITLE_BUSINESS_FINANCIAL_STANDARD,appDelegate.TITLE_BUSINESS_HUSTLE,appDelegate.TITLE_BUSINESS_VIDEOS]
        }
        else if (nav == "EVE WOMAN")
        {
            tabs = [appDelegate.TITLE_EVE_WOMAN_LIVING,appDelegate.TITLE_EVE_WOMAN_FASHION_BEAUTY,appDelegate.TITLE_EVE_WOMAN_RELATIONSHIPS, appDelegate.TITLE_EVE_WOMAN_PARENTING,appDelegate.TITLE_EVE_WOMAN_READERS_LOUNGE]
        }
        else if (nav == "ENTERTAINMENT")
        {
            tabs = [appDelegate.TITLE_ENTERTAINMENT_NEWS,appDelegate.TITLE_ENTERTAINMENT_CRAZY_MONDAY,appDelegate.TITLE_ENTERTAINMENT_PULSE, appDelegate.TITLE_ENTERTAINMENT_NAIROBIAN,appDelegate.TITLE_ENTERTAINMENT_SLIDESHOW]
        }
        
        else if (nav == appDelegate.TITLE_SPORTS)
        {
            tabs = [appDelegate.TITLE_SPORTS]
            menuBarView.isHidden = true
            
        }
        
        else if (nav == appDelegate.TITLE_PULSER)
        {
            tabs = [appDelegate.TITLE_PULSER]
            menuBarView.isHidden = true
        }
        
        else if (nav == appDelegate.TITLE_TRAVELOG)
        {
            tabs = [appDelegate.TITLE_TRAVELOG]
            menuBarView.isHidden = true
        }
        
        else if (nav == "FARMERS")
        {
            tabs = [appDelegate.TITLE_FARMERS_SMART_HARVEST,appDelegate.TITLE_FARMERS_CROP,appDelegate.TITLE_FARMERS_LIVESTOCK, appDelegate.TITLE_FARMERS_MARKET,appDelegate.TITLE_FARMERS_TECHNOLOGY,appDelegate.TITLE_FARMER_RESEARCH,appDelegate.TITLE_FARMERS_OPEDS, appDelegate.TITLE_FARMERS_ABOUT_US,appDelegate.TITLE_FARMERS_NEWS,appDelegate.TITLE_FARMERS_EVENTS]
        }
        
        navBarItem.title = nav
        
        menuBarView.dataArray = tabs
        menuBarView.isSizeToFitCellsNeeded = true
        menuBarView.collView.backgroundColor = UIColor.init(white: 0.97, alpha: 0.97)
        
        presentPageVCOnView()
        
        menuBarView.menuDelegate = self
        pageController.delegate = self
        pageController.dataSource = self
        
        //For Intial Display
        menuBarView.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .centeredVertically)
        pageController.setViewControllers([viewController(At: 0)!], direction: .forward, animated: true, completion: nil)
        
        // With CallBack Function...
        //menuBarView.menuDidSelected = myLocalFunc(_:_:)

    }
    
    
    /*
     // Call back function
    func myLocalFunc(_ collectionView: UICollectionView, _ indexPath: IndexPath) {
        
        
        if indexPath.item != currentIndex {
            
            if indexPath.item > currentIndex {
                self.pageController.setViewControllers([viewController(At: indexPath.item)!], direction: .forward, animated: true, completion: nil)
            }else {
                self.pageController.setViewControllers([viewController(At: indexPath.item)!], direction: .reverse, animated: true, completion: nil)
            }
            
            menuBarView.collView.scrollToItem(at: IndexPath.init(item: indexPath.item, section: 0), at: .centeredHorizontally, animated: true)
            
        }
        
    }
     */
 
    func presentPageVCOnView() {
        
        self.pageController = storyboard?.instantiateViewController(withIdentifier: "PageControllerVC") as! PageControllerVC
        if (nav == appDelegate.TITLE_SPORTS || nav == appDelegate.TITLE_PULSER || nav == appDelegate.TITLE_TRAVELOG){
            self.pageController.view.frame = CGRect.init(x: 0, y: navBar.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - menuBarView.frame.maxY)
        }
        else{
            self.pageController.view.frame = CGRect.init(x: 0, y: menuBarView.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - menuBarView.frame.maxY)
        }
        
        self.addChild(self.pageController)
        self.view.addSubview(self.pageController.view)
        self.pageController.didMove(toParent: self)
        
    }
    
    //Present ViewController At The Given Index
    
    func viewController(At index: Int) -> UIViewController? {
        
        if((self.menuBarView.dataArray.count == 0) || (index >= self.menuBarView.dataArray.count)) {
            return nil
        }
        
        let contentVC = storyboard?.instantiateViewController(withIdentifier: "ContentVC") as! ContentVC
        contentVC.strTitle = tabs[index]
        contentVC.pageIndex = index
        currentIndex = index
        return contentVC
        
    }
    
}





extension MainDashViewController: MenuBarDelegate {

    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {

        // If selected Index is other than Selected one, by comparing with current index, page controller goes either forward or backward.
        
        if index != currentIndex {

            if index > currentIndex {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .forward, animated: true, completion: nil)
            }else {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .reverse, animated: true, completion: nil)
            }

            menuBarView.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)

        }

    }

}


extension MainDashViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! ContentVC).pageIndex
        
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index -= 1
        return self.viewController(At: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! ContentVC).pageIndex
        
        if (index == tabs.count) || (index == NSNotFound) {
            return nil
        }
        
        index += 1
        return self.viewController(At: index)
        
    }
   
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if finished {
            if completed {
                let cvc = pageViewController.viewControllers!.first as! ContentVC
                let newIndex = cvc.pageIndex
                menuBarView.collView.selectItem(at: IndexPath.init(item: newIndex, section: 0), animated: true, scrollPosition: .centeredVertically)
                menuBarView.collView.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
