//
//  Util.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 14/01/2021.
//

import Foundation
import UIKit

var aView: UIView?

extension UIViewController {
    
    func showSpinner() {
        aView = UIView(frame: self.view.bounds)
        aView?.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
        
        let ai = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        ai.center = aView!.center
        ai.startAnimating()
        aView?.addSubview(ai)
        self.view.addSubview(aView!)
    }
    
    func hideSpinner() {
        aView?.removeFromSuperview()
        aView = nil
    }
    
    func showAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [self] action in
              switch action.style{
              case .default:
                    print("default")
                if (title == "Sign up" && message == "Successfully Registered")
                {
                    dismiss(animated: true, completion: nil)
                    
                   /* dismiss(animated: true) {
                        
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "sbLogin")
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true)
                        
                        
                    } */
                }

              case .cancel:
                    print("cancel")

              case .destructive:
                    print("destructive")


        }}))
        self.present(alert, animated: true, completion: nil)
    }
}
