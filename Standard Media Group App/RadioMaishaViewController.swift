//
//  RadioMaishaViewController.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 26/03/2021.
//

import UIKit
import SwiftAudio

class RadioMaishaViewController: UIViewController {
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    let audioPlayer = AudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let radioURL = UserDefaults.standard.string(forKey: "RadioMaisha")!
        
        let audioItem = DefaultAudioItem(audioUrl: radioURL, sourceType: .stream)
        
        do {
            
            try? audioPlayer.load(item: audioItem, playWhenReady: true) // Load the item and start playing when the player is ready.
        } catch{
            print(NSError.self)
        }
        
        audioPlayer.event.stateChange.addListener(self, handleAudioPlayerStateChange)
        
    }
    
    func handleAudioPlayerStateChange(state: AudioPlayerState) {
        // Handle the event
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
