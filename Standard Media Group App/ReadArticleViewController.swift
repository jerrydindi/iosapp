//
//  ReadArticleViewController.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 17/03/2021.
//

import UIKit

class ReadArticleViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var txtStory: UITextView!
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnShare(_ sender: Any) {
        let shareLink = UserDefaults.standard.string(forKey: "shareArticle")
        
        // set up activity view controller
                let textToShare = [ shareLink ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

                // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

                // present the view controller
                self.present(activityViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let decoded  = UserDefaults.standard.data(forKey: "fullArticle")
        let data = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [String:Any]
        
        lblTitle.text = data["title"] as! String
        var story = data["story"] as! String
       // txtStory.attributedText = story.htmlToAttributedString
        txtStory.setHTMLFromString(htmlText: story.replacingOccurrences(of: ".jpg", with: ""))
        let author = data["author"] as! String
        lblAuthor.text = "By " + author
        let thumbnail = data["thumbURL"] as! String
        imgThumb.loadImage(urlString: thumbnail)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
