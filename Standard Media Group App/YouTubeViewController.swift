//
//  YouTubeViewController.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 13/01/2021.
//

import UIKit
import youtube_ios_player_helper

class YouTubeViewController: UIViewController {
    
    @IBOutlet weak var playerView: YTPlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
      //  playerView.delegate = self
        
      //  self.showSpinner()
        
        let videoID = UserDefaults.standard.string(forKey: "YTChannel")!
        
        playerView.load(withVideoId: videoID, playerVars: ["playsinline": "1"])
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension YouTubeViewController: YTPlayerViewDelegate {
    func playerViewPreferredWebViewBackgroundColor(_ playerView: YTPlayerView) -> UIColor {
        return UIColor.black
    }
    
    func playerViewPreferredInitialLoading(_ playerView: YTPlayerView) -> UIView? {
//        let customLoadingView = UIView()
//        Create a custom loading view
//        return customLoadingView
        
     
        
        return playerView
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        //   self.hideSpinner()
       // playerView.playVideo()
    }
}
