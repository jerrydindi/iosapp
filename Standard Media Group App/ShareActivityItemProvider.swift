//
//  ShareActivityItemProvider.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 10/01/2021.
//

import Foundation
import UIKit

final class ShareActivityItemProvider: UIActivityItemProvider {
  
  let title: String
  let url: URL
  
  init(title: String, url: URL) {
    self.title = title
    self.url = url
    super.init(placeholderItem: url)
  }
  
  override var item: Any {
    return url
  }
  
  override func activityViewController(_ activityViewController: UIActivityViewController,
                                       subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
    return title
  }
  
}
