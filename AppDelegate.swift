//
//  AppDelegate.swift
//  Standard Media Group App
//
//  Created by Jeremiah Dindi on 10/12/2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    let BtnCornnerRadius : CGFloat = 20
    let BorderWidth: CGFloat = 1.0
    
   // let URL_BASE = "https://sde.co.ke/sde_lab/sg_api/v2/"
    let URL_BASE = "https://app.standardmedia.co.ke/"
    let URL_LOGIN = "login"
    let URL_REGISTER = "register"
    let URL_KTN_NEWS = "youtube_channel/UCKVsdeoHExltrWMuK0hOWmg"
    
    let URL_ARTICLES = "articles_cat/" //"articles_cat/1/1/15"
    let URL_BUSINESS_ARTICLES = "articles_business/"
    let URL_SLIDE_SHOWS = "get_slideshows/"
    let ARTICLE_NUMBER = "20"
    // The Standard
    let URL_ARTICLES_MAIN_TOP_STORIES_ID = "1"
    let URL_ARTICLES_MAIN_LOCAL_NEWS_ID = "local"
    let URL_ARTICLES_MAIN_POLITICS_ID = "3"
    let URL_ARTICLES_MAIN_ENTERTAINMENT_ID = "9"
    let URL_ARTICLES_MAIN_ENTERTAINMENT_SLIDESHOW_ID = "main"
    //The Standard Business
    let URL_ARTICLES_STANDARD_BUSINESS_FINANCIAL_STANDARD_ID = "financial_standard"
    let URL_ARTICLES_STANDARD_BUSINESS_HUSTLE_ID = "hustle"
    let URL_ARTICLES_STANDARD_BUSINESS_VIDEOS_ID = "video"
    // Sports
    let URL_ARTICLES_SPORTS_ID = "articles_sports"
    //Eve Woman
    let URL_ARTICLES_EVE_WOMAN_LIVING_ID = "articles_living_articles"
    let URL_ARTICLES_EVE_WOMAN_FASHION_BEAUTY_ID = "articles_fashion_articles"
    let URL_ARTICLES_EVE_WOMAN_RELATIONSHIPS_ID = "articles_relationships_articles"
    let URL_ARTICLES_EVE_WOMAN_PARENTING_ID = "articles_parenting_articles"
    let URL_ARTICLES_EVE_WOMAN_VIDEOS_ID = "video"
    let URL_ARTICLES_EVE_WOMAN_READERS_LOUNGE_ID = "articles_readers_articles"
    // Pulser
    let URL_ARTICLES_PULSER_ID = "articles_pulse_articles"
    // Travelog
    let URL_ARTICLES_TRAVELOG_ID = "travel_log"
    // Entertainment
    let URL_ARTICLES_ENTERTAINMENT_NEWS_ID = "articles_sde_news"
    let URL_ARTICLES_ENTERTAINMENT_CRAZY_MONDAY_ID = "articles_crazy_articles"
    let URL_ARTICLES_ENTERTAINMENT_PULSE_ID = "articles_pulse_articles"
    let URL_ARTICLES_ENTERTAINMENT_NAIROBIAN_ID = "articles_nairobian_articles"
    let URL_ARTICLES_ENTERTAINMENT_SLIDESHOW_ID = "entertainment"
    // farmers
    let URL_ARTICLES_FARMERS_SMART_HARVEST_ID = "369"
    let URL_ARTICLES_FARMERS_CROP_ID = "438"
    let URL_ARTICLES_FARMERS_LIVESTOCK_ID  = "439"
    let URL_ARTICLES_FARMERS_MARKET_ID  = "440"
    let URL_ARTICLES_FARMERS_TECHNOLOGY_ID  = "441"
    let URL_ARTICLES_FARMERS_RESEARCH_ID  = "442"
    let URL_ARTICLES_FARMERS_OPEDS_ID  = "443"
    let URL_ARTICLES_FARMERS_ABOUT_US_ID  = "444"
    let URL_ARTICLES_FARMERS_NEWS_ID  = "445"
    let URL_ARTICLES_FARMERS_EVENTS_ID  = "479"
    
    // Titles
    let TITLE_MAIN_TOP_STORIES = "TOP STORIES"
    let TITLE_MAIN_FAVOURITES = "FAVOURITES"
    let TITLE_MAIN_LOCAL_NEWS = "LOCAL NEWS"
    let TITLE_MAIN_POLITICS = "POLITICS"
    let TITLE_MAIN_ENTERTAINMENT = "ENTERTAINMENT"
    let TITLE_MAIN_SLIDE_SHOW = "SLIDE SHOW" // get_slideshows/1/20/main
    
    let TITLE_BUSINESS_FINANCIAL_STANDARD = "FINANCIAL STANDARD"
    let TITLE_BUSINESS_HUSTLE = "HUSTLE"
    let TITLE_BUSINESS_VIDEOS = "VIDEOS"
    
    let TITLE_EVE_WOMAN_LIVING = "LIVING"
    let TITLE_EVE_WOMAN_FASHION_BEAUTY = "FASHION AND BEAUTY"
    let TITLE_EVE_WOMAN_RELATIONSHIPS = "RELATIONSHIPS"
    let TITLE_EVE_WOMAN_PARENTING = "PARENTING"
    let TITLE_EVE_WOMAN_READERS_LOUNGE = "READERS LOUNGE"
    
    let TITLE_SPORTS = "SPORTS"
    
    let TITLE_ENTERTAINMENT_NEWS = "NEWS"
    let TITLE_ENTERTAINMENT_CRAZY_MONDAY = "CRAZY MONDAY"
    let TITLE_ENTERTAINMENT_PULSE = "PULSE"
    let TITLE_ENTERTAINMENT_NAIROBIAN = "NAIROBIAN"
    let TITLE_ENTERTAINMENT_SLIDESHOW = "SLIDESHOW"
    
    let TITLE_PULSER = "PULSER"
    
    let TITLE_TRAVELOG = "TRAVELOG"
    
    let TITLE_FARMERS_SMART_HARVEST = "SMART HARVEST"
    let TITLE_FARMERS_CROP = "CROP"
    let TITLE_FARMERS_LIVESTOCK = "LIVESTOCK"
    let TITLE_FARMERS_MARKET = "MARKET"
    let TITLE_FARMERS_TECHNOLOGY = "TECHNOLOGY"
    let TITLE_FARMER_RESEARCH = "RESEARCH"
    let TITLE_FARMERS_OPEDS = "OPEDS"
    let TITLE_FARMERS_ABOUT_US = "ABOUT US"
    let TITLE_FARMERS_NEWS = "NEWS"
    let TITLE_FARMERS_EVENTS = "EVENTS"
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
       // doPost(url: URL_BASE + URL_LOGIN,paramz: "email=jerrydindi@gmail.com&password=123456789")
        
        // get KTN News YouTube Channel ID
        doGET(url: URL_BASE + URL_KTN_NEWS,functn :"KTN_NEWS")
        
        // main | The Standard
        fetchArticle(type: URL_ARTICLES_MAIN_TOP_STORIES_ID)
        
        fetchArticle(type: URL_ARTICLES_MAIN_LOCAL_NEWS_ID)
        
        fetchArticle(type: URL_ARTICLES_MAIN_POLITICS_ID)
        
        fetchArticle(type: URL_ARTICLES_MAIN_ENTERTAINMENT_ID)
        
        fetchArticle(type: URL_ARTICLES_MAIN_ENTERTAINMENT_SLIDESHOW_ID)
        
        // standard business
        fetchArticle(type: URL_ARTICLES_STANDARD_BUSINESS_FINANCIAL_STANDARD_ID)
        
        fetchArticle(type: URL_ARTICLES_STANDARD_BUSINESS_HUSTLE_ID)
        
        fetchArticle(type: URL_ARTICLES_STANDARD_BUSINESS_VIDEOS_ID)
        
        // eve woman
        fetchArticle(type: URL_ARTICLES_EVE_WOMAN_LIVING_ID)
        
        fetchArticle(type: URL_ARTICLES_EVE_WOMAN_FASHION_BEAUTY_ID)
        
        fetchArticle(type: URL_ARTICLES_EVE_WOMAN_RELATIONSHIPS_ID)
        
        fetchArticle(type: URL_ARTICLES_EVE_WOMAN_PARENTING_ID)
        
        fetchArticle(type: URL_ARTICLES_EVE_WOMAN_READERS_LOUNGE_ID)
        
        // entertainment
        fetchArticle(type: URL_ARTICLES_ENTERTAINMENT_NEWS_ID)
        
        fetchArticle(type: URL_ARTICLES_ENTERTAINMENT_PULSE_ID)
        
        fetchArticle(type: URL_ARTICLES_ENTERTAINMENT_CRAZY_MONDAY_ID)
        
        fetchArticle(type: URL_ARTICLES_ENTERTAINMENT_NAIROBIAN_ID)
        
        fetchArticle(type: URL_ARTICLES_ENTERTAINMENT_CRAZY_MONDAY_ID)
        
        fetchArticle(type: URL_ARTICLES_ENTERTAINMENT_SLIDESHOW_ID)
        
        // sports
        fetchArticle(type: URL_ARTICLES_SPORTS_ID)
        
        // pulser
        fetchArticle(type: URL_ARTICLES_PULSER_ID)
        
        // travelog
        fetchArticle(type: URL_ARTICLES_TRAVELOG_ID)
        
        // farmers
        fetchArticle(type: URL_ARTICLES_FARMERS_SMART_HARVEST_ID)
        
        fetchArticle(type: URL_ARTICLES_FARMERS_CROP_ID)
        
        fetchArticle(type: URL_ARTICLES_FARMERS_LIVESTOCK_ID)
        
        fetchArticle(type: URL_ARTICLES_FARMERS_MARKET_ID)
        
        fetchArticle(type: URL_ARTICLES_FARMERS_TECHNOLOGY_ID)
        
        fetchArticle(type: URL_ARTICLES_FARMERS_RESEARCH_ID)
        
        fetchArticle(type: URL_ARTICLES_FARMERS_OPEDS_ID)
        
        fetchArticle(type: URL_ARTICLES_FARMERS_ABOUT_US_ID)
        
        fetchArticle(type: URL_ARTICLES_FARMERS_NEWS_ID)
        
        fetchArticle(type: URL_ARTICLES_FARMERS_EVENTS_ID)

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func doGET(url: String, functn: String)
    {
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "GET"

            let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [self] (data, response, error) -> Void in
                if (error != nil) {
                    print(error!)
                } else {
                    let httpResponse = response as? HTTPURLResponse
                 //   print(httpResponse!)

                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                        
                        if (functn == "KTN_NEWS")
                        {
                            decodeJSON_KTN_NEWS(json: json!)
                        }
                        else
                        {
                          //  BroadcastResponse(title: functn, message: json!)
                            decodeJSON_ARTICLES(json: json!, type: functn)
                        }
                        
                       // print (functn)
                        
                       /* if let videoId = json!["videoId"] as? String {
                            print(videoId)
                        } */
                      //  print(json)
                    } catch {
                        print(error)
                    }

                }
            })

            dataTask.resume()
    }

    func doPost(url: String, module: String, paramz: String)
    {
        let headers = [
                    "Content-Type": "application/x-www-form-urlencoded"
                ]

            let postData = NSMutableData(data: paramz.data(using: String.Encoding.utf8)!)
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data

            let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [self] (data, response, error) -> Void in
                if (error != nil) {
                    print(error!)
                } else {
                    let httpResponse = response as? HTTPURLResponse
                 //   print(httpResponse!)

                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                        
                        BroadcastResponse(title: module, message: json!)
                        
                      /*  if let error = json!["error"] as? Bool {
                            if (error == false)
                            {
                                if let user = json!["user"] as? [String: Any]
                                {
                                    print(user["name"]!)
                                    print(user["email"]!)
                                    print(user["phone"]!)
                                    print(user["id"]!)
                                }
                            }
                                    print(error)
                                } */
                        
                      //  print(json)
                    } catch {
                        print(error)
                    }

                }
            })

            dataTask.resume()
    }
    
    func login()
    {
        doPost(url: URL_BASE + URL_LOGIN,module: "login", paramz: "email=jerrydindi@gmail.com&password=123456789")
    }
    
    func BroadcastResponse(title: String, message: [String: Any])
    {
        DispatchQueue.main.async() {
            let data = [ "Topic" : title , "message": message ] as [String : Any]
            let notificationName = Notification.Name(title)
            NotificationCenter.default.post(name: notificationName, object: nil, userInfo: data)
        }
    }
    
    func decodeJSON_KTN_NEWS(json : [String: Any])
    {
        do {
            
            if let videoId = json["videoId"] as? String {
              //  print(videoId)
                UserDefaults.standard.set(videoId, forKey: "KTN_NEWS")
            }
          //  print(json)
        } catch {
            print(error)
        }
    }
    
    func fetchArticle(type: String)
    {
        if (type == URL_ARTICLES_MAIN_LOCAL_NEWS_ID)
        {
            doGET(url: URL_BASE + URL_ARTICLES + "1/21/" + ARTICLE_NUMBER, functn: type)
        }
        else if (type == URL_ARTICLES_STANDARD_BUSINESS_HUSTLE_ID || type == URL_ARTICLES_STANDARD_BUSINESS_FINANCIAL_STANDARD_ID || type == URL_ARTICLES_STANDARD_BUSINESS_VIDEOS_ID) {
            doGET(url: URL_BASE + URL_BUSINESS_ARTICLES + type + "/1/" + ARTICLE_NUMBER, functn: type)
        }
        else if (type == URL_ARTICLES_EVE_WOMAN_LIVING_ID || type == URL_ARTICLES_EVE_WOMAN_PARENTING_ID || type == URL_ARTICLES_EVE_WOMAN_RELATIONSHIPS_ID || type == URL_ARTICLES_EVE_WOMAN_READERS_LOUNGE_ID || type == URL_ARTICLES_EVE_WOMAN_FASHION_BEAUTY_ID || type == URL_ARTICLES_EVE_WOMAN_VIDEOS_ID || type == URL_ARTICLES_ENTERTAINMENT_NAIROBIAN_ID || type == URL_ARTICLES_ENTERTAINMENT_NEWS_ID || type == URL_ARTICLES_ENTERTAINMENT_PULSE_ID || type == URL_ARTICLES_ENTERTAINMENT_CRAZY_MONDAY_ID || type == URL_ARTICLES_ENTERTAINMENT_SLIDESHOW_ID || type == URL_ARTICLES_SPORTS_ID || type == URL_ARTICLES_TRAVELOG_ID || type == URL_ARTICLES_PULSER_ID)
        {
            doGET(url: URL_BASE + type + "/1/" + ARTICLE_NUMBER, functn: type)
        }
        /* else if (type == URL_ARTICLES_ENTERTAINMENT_SLIDESHOW_ID || type == URL_ARTICLES_MAIN_ENTERTAINMENT_SLIDESHOW_ID) // slide shows
        {
            doGET(url: URL_BASE + URL_SLIDE_SHOWS + "/1/" + ARTICLE_NUMBER + "/" + type, functn: type)
        } */
        else{
            doGET(url: URL_BASE + URL_ARTICLES + type + "/1/" + ARTICLE_NUMBER, functn: type)
        }
        
    }
    
    func decodeJSON_ARTICLES(json : [String: Any], type: String)
    {
        do {
            
            if let error = json["error"] as? Int32 {
                if (error == 0)
                {
                    if let articles = json["articles"] as? [[String: Any]]
                    {
                       /* let category = JobCategory(id: 1, name: "Test Category", URLString: "http://www.example-job.com")
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: category, requiringSecureCoding: false)
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(encodedData, forKey: UserDefaultsKeys.jobCategory.rawValue)
                        
                        let decoded  = UserDefaults.standard.object(forKey: UserDefaultsKeys.jobCategory.rawValue) as! Data
                        let decodedTeams = NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as! JobCategory
                        print(decodedTeams.name) */
                        
                        var userDefaults = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: articles)
                        userDefaults.set(encodedData, forKey: type)
                        userDefaults.synchronize()
                        
                     //   UserDefaults.standard.set(articles, forKey: type)
                        
                      /*  if let article = articles[0] as? [String: Any]
                        {
                            print(article["author"])
                        } */
                    }
                }
                     //   print(error)
                    }
           // print(json)
        } catch {
            print(error)
        }
    }

}

